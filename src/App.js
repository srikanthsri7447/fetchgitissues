import React, { Component } from 'react'
import RepositoryItem from './Components/RepositoryItem'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fetchedData: [],
      isLoading: true
    }
  }

  componentDidMount = () => {

    fetch("https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories")
      .then((data) => data.json())
      .then((data) => {
        this.setState({
          fetchedData: data.items,
          isLoading: false
        })
      })

  }

  render() {

    console.log('render')

    {
      return this.state.isLoading ?
        <h1>is Loading... </h1> :
        <>
          <div className='continer-fluid d-flex flex-wrap'>
            <div className='row'>
              {this.state.fetchedData.map((eachRepo, index) => {
                return <RepositoryItem details={eachRepo} serial={index} />
              })}
            </div>
          </div>
        </>
    }



  }
}

export default App