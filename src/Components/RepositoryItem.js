import React from 'react'

import '../App.css'

function RepositoryItem(props) {

    return (
        <div className='col-12 col-lg-3 m-1 p-1 d-flex flex-column align-items-center itemContainer'>
            <h1>#{props.serial + 1}</h1>
            <div className='w-50'>
            <img src={props.details.owner.avatar_url} alt='avatar' className='w-100' />
            </div>
            <h2 className='mt-1'>{props.details.owner.login}</h2>
            <ul className='listItems'>
                <li><span className='userIcon'><i class="fa-solid fa-user"></i></span>{props.details.owner.login}</li>
                <li><span className='starIcon'><i class="fa-solid fa-star"></i></span>{props.details.watchers}</li>
                <li> <span className='forkIcon'><i class="fa-solid fa-code-fork"></i></span>{props.details.forks}</li>
                <li><span className='issueIcon'><i class="fa-solid fa-triangle-exclamation"></i></span>{props.details.open_issues}</li>
            </ul>

        </div>
    )
}

export default RepositoryItem